var express = require('express');
var app = express();
var fs = require("fs");
var data = '';

var read = fs.createReadStream('kaokonlakao.txt');

read.setEncoding('UTF8');

read.on('data', function (chunk) {
    data += chunk;
});

read.on('end', function () {
    app.get('/', function (request, response) {
        response.send('<pre>' + data.toString() + '</pre>');
    });
});

read.on('error', function (err) {
    console.log(err.stack);
});

var server = app.listen(8080, function () {
    console.log('Server running at http://localhost:8080/');
})
