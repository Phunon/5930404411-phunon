var express = require('express');
var app = express();
var fs = require("fs");

const jsonfile = require('jsonfile')

const file = 'data.json'
const obj = {
    "name": "CM",
    "courses": [
        "198330",
        "198371"
    ],
    "places": {
        "residence": "Khon Kaen",
        "visitits": [
            "songkla",
            "Bangkok"
        ]
    }
}

jsonfile.writeFile(file, obj, function (err) {
    if (err) console.error(err)
})

app.get('/', function (request, response) {
    fs.readFile(file, function(err,data) {
        console.log(data);
        response.end(data);
    });
});

var server = app.listen(8080, function () {
    console.log('=== The values of the the second course and the residence ===');
    console.log('Studying ' + obj.courses[1] + " living in " + obj.places.residence);
})
