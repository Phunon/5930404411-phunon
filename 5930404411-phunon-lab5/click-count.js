var buttons = document.querySelectorAll('button');
var para = document.createElement('p');
var count = 1;

function createParagraph() {
    if (count == 1) {
        para.textContent = 'You clicked the button for 1 time';
        document.body.appendChild(para);
    } else {
        para.textContent = 'You clicked the button for ' + count + ' times';
        document.body.appendChild(para);
    }
    count++;
}

for (var i = 0; i < buttons.length; i++) {
    buttons[i].addEventListener('click', createParagraph);
}