'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PersonSchema = new Schema({
	country: {
		type: String,
		default: 'Thailand'
	},
	name: {
		type: String,
		required: ''
	},
	weight: {
		type: Number,
		required: ''
	}
});

module.exports = mongoose.model('Persons', PersonSchema);

