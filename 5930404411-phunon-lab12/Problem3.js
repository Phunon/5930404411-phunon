var request1 = require("request");
var express = require('express');
var app = express();
var http = require("http");

request1.post("https://maps.googleapis.com/maps/api/directions/json?origin=Khon%20Kaen&destination=Bangkok&key=AIzaSyAXkeEn3sy--_ir3tY6z96h2TVWvO7yHVo", (error, response, body) => {
    if (error) {
        return console.dir(error);
    }
    var resultObj = JSON.parse(body);
    var results = resultObj.routes[0].legs[0].steps;
    http.createServer(function (req, res) {
        res.writeHead(200, {
            "content-type": "text/html;charset=utf-8"
        });
        res.write("<h1>Directions from Khon Kaen to Bangkok</h1>");
        res.write("<ol>");
        for (i = 0; i < results.length; i++) {
            res.write("<li>" + results[i].html_instructions + "</li>");
            res.write('<distance style="color:blue" >(' + results[i].distance.text + ")</distance>");
        }
        res.write("</ol>");
    }).listen(8080);
});
