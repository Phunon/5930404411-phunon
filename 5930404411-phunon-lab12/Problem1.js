var request1 = require("request");
var express = require('express');
var app = express();
var http = require("http");

request1.post("https://maps.googleapis.com/maps/api/place/textsearch/json?query=restaurants%20in%20Khon%20Kaen&key=AIzaSyD8Pprn7OZ7qCd-WEAiFkIJd1rm3X9qiuE", (error, response, body) => {
    if (error) {
        return console.dir(error);
    }
    var resultObj = JSON.parse(body);
    var results = resultObj.results;
    http.createServer(function (req, res) {
        res.writeHead(200, {
            "content-type": "text/html;charset=utf-8"
        });
        res.write("<h1>Restaurants in Khon Kaen</h1>");
        res.write("<ol>");
        for (i = 0; i < results.length; i++) {
            res.write("<li>" + results[i].name + "</li>");
        }
        res.write("</ol>");
    }).listen(8080);
});
